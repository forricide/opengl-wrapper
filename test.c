#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glu.h>
#include <GL/glut.h>

int m_glutInit() {
//	centerOnScreen();
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	return glutCreateWindow("My Window!");
}

void main_entry(int state) {
	if (state == GLUT_ENTERED) printf("Mouse in window.\n");
	else if (state == GLUT_LEFT) printf("Mouse not in window.\n");
}

int main(int argc, char * argv[]) {
	glutInit(&argc, argv);
	int window = m_glutInit();
	
	glutMainLoop();
}
